//
//  NextViewController.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import UIKit

class NextViewController: UIViewController {
    
    var modelCoreData: [UserList] = []
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            let nib = UINib(nibName: FirstTableViewCell.nib , bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: FirstTableViewCell.identifier)
            tableView.rowHeight = 75
            
        }
    }
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getData()
    }
    
    func getData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        do {
            modelCoreData = try context.fetch(UserList.fetchRequest())
        } catch  {
            print("heello")
        }
        tableView.reloadData()
    }
    

}

extension NextViewController: UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelCoreData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FirstTableViewCell.identifier, for: indexPath) as! FirstTableViewCell
        cell.accessoryType = .disclosureIndicator
        cell.configureCoreData(coreData: modelCoreData[indexPath.row])
        return cell
        }
    
    
        
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        if editingStyle == .delete {
            let task = modelCoreData[indexPath.row]
            context.delete(task)
            modelCoreData.remove(at: indexPath.row)
            (UIApplication.shared.delegate as! AppDelegate).saveContext()
            
        }
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "secondVC") as! SecondViewController
        let task = modelCoreData[indexPath.row]
        vc.person?.name?.first = task.firstName
        vc.person?.name?.last = task.lastName
        vc.person?.picture?.large = task.photo
        vc.person?.phone = task.phoneNumber
        vc.person?.email = task.email

        navigationController?.pushViewController(vc, animated: true)
    }
    }


    
    
