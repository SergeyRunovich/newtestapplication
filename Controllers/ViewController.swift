//
//  ViewController.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import UIKit

class ViewController: UIViewController {
    var modelData: ModelApi?
    let network = Network()
    var modelDataArray: [Result] = []
    var currentPage = 1
    @IBOutlet weak var tableView: UITableView!{
        didSet {
            tableView.delegate = self
            tableView.dataSource = self
            let nib = UINib(nibName: FirstTableViewCell.nib , bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: FirstTableViewCell.identifier)
            tableView.rowHeight = 75
            
        }
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        fetchData()
       
    }

    func fetchData() {
        network.fetchData() { (data) in
            //self.modelData?.results? += data?.results ?? []
            self.modelDataArray += data?.results ?? []
            //print(data)
            DispatchQueue.main.async {
                self.tableView.reloadData()
                
            }
        }
    }
    

}

extension ViewController: UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelDataArray.count
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "secondVC") as! SecondViewController
        vc.person = modelDataArray[indexPath.row]
     

        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: FirstTableViewCell.identifier, for: indexPath) as! FirstTableViewCell
        cell.configur(result: modelDataArray[indexPath.row])
        cell.accessoryType = .disclosureIndicator
           
//        if indexPath.row == modelDataArray.count - 1 && currentPage < modelData?.results?.count ?? 1 {
//            currentPage += 1
//            fetchData()
//        }
        if indexPath.row == modelDataArray.count - 3 {
            fetchData()
        }
        
        
        return cell
        
    }
    
}
    
    
