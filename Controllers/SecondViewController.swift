//
//  SecondViewController.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import UIKit
import CoreData
class SecondViewController: UIViewController {
    
 
    let network = Network()
    var person: Result?
    var imageView: UIImage?
    var saveButton = UIBarButtonItem()
    
    
   
    
    @IBOutlet weak var editTableView: UITableView!{
        didSet {
            editTableView.delegate = self
            editTableView.dataSource = self
            let nib = UINib(nibName: EditProfileUser.nib , bundle: nil)
            editTableView.register(nib, forCellReuseIdentifier: EditProfileUser.identifier)
            let nib2 = UINib(nibName: PhotoEditUser.nib, bundle: nil)
            editTableView.register(nib2, forCellReuseIdentifier: PhotoEditUser.identifier)
            editTableView.tableFooterView = UIView()
            
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        saveButtonPress()
    }
    
    
    
    func saveButtonPress() {
        saveButton.title = "Save"
        saveButton.style = .plain
        saveButton.target = self
        saveButton.action = #selector(buttonTaped)
        navigationItem.rightBarButtonItem = saveButton
    }
    
   @objc func buttonTaped() {
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let userData = UserList(context:context)
    userData.firstName = person?.name?.first
    userData.lastName = person?.name?.last
    userData.email = person?.email
    userData.phoneNumber = person?.phone
    userData.photo = person?.picture?.large
    (UIApplication.shared.delegate as! AppDelegate).saveContext()
    
    tabBarController?.selectedIndex = 1
    }
    
    
    
    func changePickerController() {
        let vc = UIImagePickerController()
        vc.sourceType = .photoLibrary
        vc.delegate = self
        vc.allowsEditing = true
        present(vc, animated: true)
        
    }
    
  
    
    
}




extension SecondViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerEditedImage")] as? UIImage {
            imageView = image
            //print(imageView)
        }
       
        picker.dismiss(animated: true, completion: nil)
        editTableView.reloadData()
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension SecondViewController: UITableViewDelegate , UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            
            let cell2 = tableView.dequeueReusableCell(withIdentifier: PhotoEditUser.identifier, for: indexPath) as! PhotoEditUser
            
            
            cell2.callback = {
                self.changePickerController()
            }
            
           // cell2.configur(result: modelDataArray[indexPath.row])
            if imageView == nil {
                cell2.configur(result: person)
            } else {
                cell2.configurPhoto(image: imageView)
            }
            return cell2
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: EditProfileUser.identifier, for: indexPath) as! EditProfileUser
            cell.configur(result: person)
            return cell
        default:
            return UITableViewCell()
           
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 200        }
        else if  indexPath.row == 1{
            return 200
        }
        
        return 0
    }
}
