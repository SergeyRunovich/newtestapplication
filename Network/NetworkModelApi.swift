//
//  NetworkModelApi.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import Foundation

class Network   {
    func fetchData(complition: @escaping (ModelApi?) -> Void ) {
        if let url = URL(string:
                            "https://randomuser.me/api/?results=20") {
            let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
                if let error = error {
                    print(error)
                }
                if let response = response {
                
                }
                if let data = data {
                    let modelTest = try! JSONDecoder().decode(ModelApi.self, from: data)
                    complition(modelTest)
                    
                }
                
            
                
                
                
            }
            
            task.resume()
        }
    }
}
