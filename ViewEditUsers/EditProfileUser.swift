//
//  EditProfileUser.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 10.03.21.
//

import UIKit

class EditProfileUser: UITableViewCell {
    
   
        
    
    
    @IBOutlet weak var firstNameLabel: UITextField!
    @IBOutlet weak var lastNameLabel: UITextField!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var phoneLabel: UITextField!
    
    static var identifier = "editCell"
    static var nib = "EditProfileUser"
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    
    func configur(result: Result?) {
        guard let result = result else { return }
        print(result.name?.first)
        firstNameLabel.text = result.name?.first
        lastNameLabel.text = result.name?.last
        emailLabel.text = result.email
        phoneLabel.text = result.phone
        
        }
   
    
}
