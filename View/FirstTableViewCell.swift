//
//  FirstTableViewCell.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import UIKit
import Kingfisher
class FirstTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageTeamLabel: UIImageView!
    @IBOutlet weak var lastName: UILabel!
    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var firstName: UILabel!
    
    
    static var identifier = "infoCell"
    static var nib = "FirstTableViewCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageTeamLabel.layer.cornerRadius = imageTeamLabel.frame.height/2
    }

    
    func configur(result: Result?) {
        guard let result = result else { return }
        
        firstName.text = result.name?.first
        lastName.text = result.name?.last
        
        if let phone = result.phone {
            number.text = "\(phone)"
        }
        if let image = URL(string: result.picture?.large ?? "") {
            imageTeamLabel.kf.setImage(with: image)
        }
    }
        
        func configureCoreData(coreData: UserList) {
            firstName.text = coreData.firstName
            lastName.text = coreData.lastName
            
            if let phone = coreData.phoneNumber {
                number.text = "\(phone)"
            }
            if let image = URL(string: coreData.photo ?? "") {
                imageTeamLabel.kf.setImage(with: image)
            }
            

        }
        
        
        
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
