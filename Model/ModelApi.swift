//
//  ModelApi.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 9.03.21.
//

import Foundation

// MARK: - Welcome
struct ModelApi: Codable {
    var results: [Result]?
   
}

// MARK: - Info


// MARK: - Result
struct Result: Codable {
    
    var name: Name?
    var email: String?
    var phone: String?
    var picture: Picture?

}



// MARK: - Name
struct Name: Codable {
    var title, first, last: String?
}

// MARK: - Picture
struct Picture: Codable {
    var large, medium, thumbnail: String?
}
