//
//  PhotoEditUser.swift
//  TestApplication2
//
//  Created by Сергей Рунович on 10.03.21.
//

import UIKit
import Kingfisher
class PhotoEditUser: UITableViewCell {

    @IBOutlet weak var imagePhotoView: UIImageView!
    
    var callback: (() -> ())?
    
   
    
    
    
    static var identifier = "photoCell"
    static var nib = "PhotoEditUser"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imagePhotoView.layer.cornerRadius = imagePhotoView.frame.height/2
    }

    @IBAction func changeButton(_ sender: Any) {
        
        if let finish = callback {
            finish()
        }
    }
    
    
    func configurPhoto(image: UIImage?) {
        imagePhotoView.image = image

    }
    
    func configur(result: Result?) {
        guard let result = result else { return }
       
        if let image = URL(string: result.picture?.large ?? "") {
       imagePhotoView.kf.setImage(with: image)
        }

    }
        

        // Configure the view for the selected state
    }
    


